import sbt._
import Keys._
import sbtassembly.AssemblyPlugin.autoImport._

mainClass in assembly := None

jarName in assembly := "portForwarder-all.jar"

val distrib = TaskKey[File]("distrib", "Creates a distributable zip file.")

distrib := {
  val fatJar: File = assembly.value
  val baseDir = baseDirectory.value
  val target = Keys.target.value
  val name = Keys.name.value
  val version = Keys.version.value

  val zipFile = target / s"$name.zip"
  IO.delete(zipFile)

  val srcMain = baseDir / "src" / "main"
  val binEntries = IO.listFiles(srcMain / "bin").map(f => f -> s"bin/${f.getName}")
  val confEntries = IO.listFiles(srcMain / "conf").map(f => f -> s"conf/${f.getName}")
  val allZipEntries = (fatJar -> s"lib/${fatJar.getName}") :: confEntries.toList ::: binEntries.toList

  IO.zip(allZipEntries, zipFile)
  zipFile
}
