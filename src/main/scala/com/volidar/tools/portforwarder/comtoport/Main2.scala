package com.volidar.tools.portforwarder.comtoport

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.io.Tcp.SO.TcpNoDelay
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import com.volidar.tools.portforwarder.common
import com.volidar.tools.portforwarder.common.{Data, Disconnect, Message, Noop}
import jssc.SerialPort

import scala.concurrent.duration._
import scala.util.Try

object Main2 extends LazyLogging {
  val comPort = "COM1"

  def main(args: Array[String]) {
    val serialPort = new SerialPort(comPort)
    serialPort.openPort();
    serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

    val system = ActorSystem("comToPort")
    val serialWriter = system.actorOf(SerialWriterActor.props(serialPort))
    val dispatcher = system.actorOf(DispatcherActor.props(serialWriter))

    Try {
      var buffer = Array.empty[Byte]
      while(true) {
        serialPort.readBytes() match {
          case null => Thread.sleep(1L)
          case newBytes =>
            val (newBuffer, messages) = Message.fromBytes(buffer ++ newBytes)
            buffer = newBuffer
            messages.filter(_ != Noop).foreach {
              case message =>
                logger.trace(s"-> $message")
                dispatcher ! message
            }
        }
      }
    }
    system.terminate()
  }
}

object SerialWriterActor {
  def props(serial: SerialPort) = Props(classOf[SerialWriterActor], serial)
}
class SerialWriterActor(serial: SerialPort) extends Actor with LazyLogging {
  val noopSchedule = context.system.scheduler.schedule(100.milliseconds, 100.milliseconds, self, Noop)(context.dispatcher)

  override def postStop() = noopSchedule.cancel()

  override def receive = {
    case message: Message =>
      if(message != Noop) logger.trace(s"<- $message")
      serial.writeBytes(Message.toBytes(message))
  }
}

object DispatcherActor {
  def props(serialWriter: ActorRef) = Props(classOf[DispatcherActor], serialWriter)
  val doTrace = false
}
class DispatcherActor(serialWriter: ActorRef) extends Actor with LazyLogging {
  var waitingSockets = Map.empty[InetSocketAddress, List[Int]]
  var connections = Map.empty[Int, ActorRef]

  override def receive = {
    case msg@com.volidar.tools.portforwarder.common.Connect(connectionId, host, port) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      val targetAddress = new InetSocketAddress(host, port)
      waitingSockets.get(targetAddress) match {
        case None => waitingSockets += (targetAddress -> List(connectionId))
        case Some(waitingConnectionIds) => waitingSockets += (targetAddress -> (connectionId :: waitingConnectionIds))
      }
      IO(Tcp)(context.system) ! Connect(targetAddress, options = Set(TcpNoDelay(true)))
    case msg@CommandFailed(connect: Connect) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      removeWaitingSocket(connect.remoteAddress).foreach(connectionId => serialWriter ! Disconnect(connectionId, fromClient = false))
    case msg@Connected(remoteAddress, _) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      removeWaitingSocket(remoteAddress) match {
        case None => sender() ! Close
        case Some(connectionId) =>
          val connection = context.actorOf(ConnectionActor.props(connectionId, sender(), self))
          connections += (connectionId -> connection)
          serialWriter ! common.Connected(connectionId)
      }

    case msg@Data(connectionId, true, _) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      connections.get(connectionId) match {
        case None => serialWriter ! Disconnect(connectionId, false)
        case Some(connection) => connection ! msg
      }
    case msg@Disconnect(connectionId, true) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      waitingSockets = waitingSockets.map { case (targetAddress, connectionIds) => (targetAddress, connectionIds.filter(_ != connectionId)) }
      connections.get(connectionId).foreach {
        case connection =>
          connection ! msg
          connections -= connectionId
      }

    case msg@Data(_, false, _) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      serialWriter ! msg
    case msg@Disconnect(connectionId, false) =>
      if(DispatcherActor.doTrace) logger.trace(s"d> $msg")
      connections -= connectionId
      serialWriter ! msg
  }

  def removeWaitingSocket(remoteAddress: InetSocketAddress): Option[Int] = {
    waitingSockets.get(remoteAddress).map {
      case Nil => throw new Exception("Corrupted data") //this should never occur
      case singleConnectionId :: Nil =>
        waitingSockets -= remoteAddress
        singleConnectionId
      case lastConnectionId :: otherConnectionIds =>
        waitingSockets += (remoteAddress -> otherConnectionIds)
        lastConnectionId
    }
  }
}

object ConnectionActor {
  def props(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) = Props(classOf[ConnectionActor], connectionId, socket, dispatcher)
}
class ConnectionActor(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) extends Actor with LazyLogging {
  socket ! Register(self, false, true)

  override def receive = {
    case Data(_, true, data) => socket ! Write(ByteString(data))
    case msg@Disconnect(_, true) =>
      socket ! Close
      context.stop(self)

    case CommandFailed(_: Write) =>
      logger.error("CommandFailed(_: Write) is not expected in this implementation")
      dispatcher ! Disconnect(connectionId, fromClient = false)
      socket ! Close
      context.stop(self)
    case Received(data) =>
      dispatcher ! Data(connectionId, fromClient = false, data.toArray)
    case _: ConnectionClosed =>
      dispatcher ! Disconnect(connectionId, fromClient = false)
      context.stop(self)

    case other => logger.warn(s"Unexpected $other")
  }
}
