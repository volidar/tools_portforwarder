package com.volidar.tools.portforwarder.comtoport

import java.io.IOException
import java.net.Socket

import com.typesafe.scalalogging.LazyLogging
import jssc.SerialPort

import scala.util.Try

//object Main extends LazyLogging {
//  val comPort = "COM1"
//
//  def main(args: Array[String]) {
//    val serialPort = new SerialPort(comPort)
//    serialPort.openPort();
//    serialPort.setParams(115200, 8, 1, 0);
//    while(true) {
//      serialPort.readBytes() match {
//        case null =>
//          serialPort.writeByte(0)
//          Thread.sleep(100L)
//        case buffer =>
//          logger.trace(s"${buffer.length}: ${new String(buffer, "UTF-8")}")
//          buffer.grouped(255).foreach{
//            case group =>
//              serialPort.writeByte(group.length.toByte)
//              serialPort.writeBytes(group)
//          }
//      }
//    }
//  }
//}
object Main extends LazyLogging {
  val comPort = "COM1"
  val targetHost = "192.168.168.131"
  val targetPort = 3389

  val serialPort = new SerialPort(comPort)
  serialPort.openPort();
  serialPort.setParams(115200, 8, 1, 0);

  def writeSerial(data: Array[Byte]) = {
    synchronized {
      serialPort.writeByte(data.length.toByte)
      serialPort.writeBytes(data)
    }
  }

  def main(args: Array[String]) {
    try{
      val buffer = new Array[Byte](32768)
      while(true) {
        val socket = new Socket(targetHost, targetPort)
        logger.debug("Connection opened")
        val comToSocketThread = new ComToSocket(socket)
        comToSocketThread.start()
        try {
          val in = socket.getInputStream
          while(in.read(buffer) match {
            case -1 => false
            case read =>
              logger.trace(s"-> $read")
              buffer.take(read).grouped(255).foreach(writeSerial)
              true
          }) {}
        }
        catch {
          case ex: IOException => logger.debug("Exception in Socket -> Com", ex)
        }
        finally Try {socket.close()}
        Try {
          comToSocketThread.interrupt()
          comToSocketThread.join()
        }
        logger.debug("Connection closed")
      }
    } finally Try {serialPort.closePort()}
  }

  class ComToSocket(target: Socket) extends Thread {
    val out = target.getOutputStream
    override def run() = {
      try {
        while(!isInterrupted) {
          serialPort.readBytes() match {
            case null =>
              writeSerial(Array.empty[Byte])
              Thread.sleep(10L)
            case buffer =>
              logger.trace(s"<- ${buffer.length}")
              out.write(buffer)
          }
        }
      }
      catch {
        case ex: IOException => logger.debug("Exception in Com -> Socket", ex)
      } finally {
        Try {target.close()}
      }
    }
  }
}
