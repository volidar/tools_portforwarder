package com.volidar.tools.portforwarder.porttoport

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import com.volidar.tools.portforwarder.porttoport.MonitorActor.WriteDuration

object MonitorActor {
  def props = Props(classOf[MonitorActor])
  case class WriteDuration(duration: Long, byteCount: Long)
}
class MonitorActor extends Actor with LazyLogging {
  var writeDuration = 0L
  var writeBytes = 1L
  var writeCount = 1L
  var writeMaxDuration = 0L
  var writeMaxBytes = 0L

  override def receive = {
    case _: WriteDuration => ;
  }
  if(logger.underlying.isTraceEnabled()) context.become(printingBehavior)

  def printingBehavior: Receive = {
    case WriteDuration(duration, byteCount) =>
      writeDuration += duration
      writeBytes += byteCount
      writeCount += 1
      if(duration > writeMaxDuration) {
        writeMaxDuration = duration
        writeMaxBytes = byteCount
      }
      printStats()
  }

  var nextStatusTime = 0L
  def printStats(): Unit = {
    val now = System.currentTimeMillis()
    if(nextStatusTime < now) {
      print(f"\rtotalBytes = ${bytes(writeBytes)}%s; speed/s = ${bytes((1000000000.0 * writeBytes / writeDuration).toLong)}%s; avg(/send) = ${writeDuration / writeCount}%9d; avg(/byte) = ${
        writeDuration / writeBytes
      }%9d; max(write) = $writeMaxDuration%10d for $writeMaxBytes%d bytes       ")
      nextStatusTime = now + 1000L
    }
  }
  val modifiers = List("", "k", "M", "G", "T", "P")
  def bytes(value: Long): String = {
    def bytes(value: Long, modifiers: List[String]): String = {
      if(modifiers.length == 1 || value < 128 * 1024) {
        f"$value%6d${modifiers.head}%s"
      } else {
        bytes(value / 1024, modifiers.tail)
      }
    }
    bytes(value, modifiers)
  }
}
