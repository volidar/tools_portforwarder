package com.volidar.tools.portforwarder.porttoport

import java.net.{ServerSocket, Socket}

import com.typesafe.scalalogging.LazyLogging

import scala.reflect.internal.util.Statistics

object LoopTester extends LazyLogging {
  def main(args: Array[String]) {
    val serverSocket = new ServerSocket(4321)
    val start = new Socket("127.0.0.1", 1234)
//    val start = new Socket("192.168.168.130", 1234)
    val end = serverSocket.accept();
    var startToEndStatistics = Statistics(0L, 0L, 0L)
    var endToStartStatistics = Statistics(0L, 0L, 0L)
    while(true) {
      startToEndStatistics = sendReceive(start, end, startToEndStatistics, generateData)
      endToStartStatistics = sendReceive(end, start, endToStartStatistics, generateData)
      print(s"\r$startToEndStatistics <-> $endToStartStatistics")
    }
  }

  def generateData: Array[Byte] = new Array[Byte]((math.random * 1024).toInt + 1).map(_ => (math.random * 256).toByte)

  case class Statistics(totalTime: Long, totalBytes: Long, sendCount: Long) {
//    print(f"\rAvg per send: ${totalTime / sendCount}%9d; avg per byte: ${totalTime / totalBytes}%9d; this: ${duration}%9d")
    override def toString = f"ns/send: ${totalTime / sendCount}%9d; ns/byte: ${totalTime / totalBytes}%9d; byte/s: ${totalBytes * 1000000000 / totalTime}%9d"
  }

  def sendReceive(sender: Socket, reader: Socket, statistics: Statistics, data: Array[Byte]): Statistics = {
    val startTime = System.nanoTime();
    val out = sender.getOutputStream
    val in = reader.getInputStream
    val buffer = new Array[Byte](data.length)
    var pos = 0

    data.grouped(1024).foreach{
      case packet =>
        out.write(packet)
        out.flush() //DOIT try without it
        while(in.available() > 0 && pos < buffer.length) {
          val read = in.read(buffer, pos, buffer.length - pos)
          if(read == -1) throw new Exception("Closed Socket")
          pos += read
        }
    }

    while(pos < buffer.length) {
      val read = in.read(buffer, pos, buffer.length - pos)
      if(read == -1) throw new Exception("Closed Socket")
      pos += read
    }
    assert(buffer.deep == data.deep)
    val duration = System.nanoTime() - startTime
    Statistics(statistics.totalTime + duration, statistics.totalBytes + data.length, statistics.sendCount + 1)
  }
}
