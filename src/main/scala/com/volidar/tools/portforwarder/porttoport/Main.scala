package com.volidar.tools.portforwarder.porttoport

import java.net.InetSocketAddress

import akka.actor._
import akka.io.Tcp.SO.TcpNoDelay
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import com.typesafe.scalalogging.{LazyLogging, Logger}
import com.volidar.tools.portforwarder.porttoport.ForwarderActor.WriteAck
import com.volidar.tools.portforwarder.porttoport.MonitorActor.WriteDuration

object Main extends LazyLogging {
  case class InvalidArgumentException(argument: String) extends Exception
  case object MissingArgumentsException extends Exception

  def main(args: Array[String]): Unit = {
    try {
      val forwards = args.toList.map {
        case forwardString => forwardString.split(":").toList match {
          case sourcePort :: targetServer :: targetPort :: Nil => (sourcePort.toInt, targetServer, targetPort.toInt)
          case _ => throw InvalidArgumentException(forwardString)
        }
      }
      if(forwards.isEmpty) throw MissingArgumentsException
      val system = ActorSystem("portForwarder")
      val monitorActor = system.actorOf(MonitorActor.props)
      forwards.foreach {
        case (sourcePort, targetServer, targetPort) => system.actorOf(ListenerActor.props(sourcePort, targetServer, targetPort, monitorActor))
      }
    }
    catch {
      case InvalidArgumentException(argument) =>
        println(s"Wrong argument: $argument")
        println("Expected arguments: (sourcePort:targetServer:targetPort)+")
      case MissingArgumentsException =>
        println("Expected arguments: (sourcePort:targetServer:targetPort)+")
    }
  }
}

object ListenerActor {
  def props(sourcePort: Int, targetServer: String, targetPort: Int, monitorActor: ActorRef) = Props(classOf[ListenerActor], sourcePort, targetServer, targetPort, monitorActor)
}

class ListenerActor(sourcePort: Int, targetServer: String, targetPort: Int, monitorActor: ActorRef) extends Actor with LazyLogging {
  IO(Tcp)(context.system) ! Bind(self, new InetSocketAddress(sourcePort))
  override def receive = {
    case CommandFailed(_: Bind) =>
      println(s"Cannot bind on port $sourcePort")
      context.system.terminate()
    case _: Bound => logger.trace(s"Bound $sourcePort -> $targetServer:$targetPort");
    case Connected(clientAddress, myServerAddress) =>
      logger.trace(s"Connected from $clientAddress to me at $myServerAddress")
      context.actorOf(TargetConnectorActor.props(sender(), targetServer, targetPort, monitorActor, clientAddress))
  }
}

object TargetConnectorActor {
  def props(clientConnection: ActorRef, targetServer: String, targetPort: Int, monitorActor: ActorRef, from: InetSocketAddress) =
    Props(classOf[TargetConnectorActor], clientConnection, targetServer, targetPort, monitorActor, from)
}

class TargetConnectorActor(clientConnection: ActorRef, targetServer: String, targetPort: Int, monitorActor: ActorRef, from: InetSocketAddress) extends Actor with LazyLogging {

  import scala.concurrent.duration._

  IO(Tcp)(context.system) ! Connect(new InetSocketAddress(targetServer, targetPort), options = Set(TcpNoDelay(true)), timeout = Some(1.day))

  override def receive = {
    case CommandFailed(_: Connect) =>
      logger.warn(s"Cannot connect to $targetServer:$targetPort")
      clientConnection ! Close
      context.stop(self)
    case Connected(serverAddress, myClientAddress) =>
      logger.trace(s"Connected to $serverAddress from me at $myClientAddress")
      context.actorOf(ForwarderActor.props(clientConnection, sender(), "s", monitorActor, (from, myClientAddress, serverAddress)))
      context.actorOf(ForwarderActor.props(sender(), clientConnection, "t", monitorActor, (from, myClientAddress, serverAddress)))
  }
}

object ForwarderActor {
  def props(fromConnection: ActorRef, toConnection: ActorRef, direction: String, monitorActor: ActorRef, fromOverTo: (InetSocketAddress, InetSocketAddress, InetSocketAddress)) =
    Props(classOf[ForwarderActor], fromConnection, toConnection, direction, monitorActor, fromOverTo)
  case class WriteAck(startTime: Long, byteCount: Long) extends Tcp.Event
}
class RawLogger(route: String) extends LazyLogging {
  def apply(msg: => String) = logger.info(s"$route $msg")
}
class ForwarderActor(fromConnection: ActorRef, toConnection: ActorRef, direction: String, monitorActor: ActorRef, fromOverTo: (InetSocketAddress, InetSocketAddress, InetSocketAddress)) extends Actor with Stash with LazyLogging {
  val rawLogger = new RawLogger(s"${fromOverTo._1} -- ${fromOverTo._2} -> ${fromOverTo._3} $direction")

  fromConnection ! Register(self, false, true)
  logger.debug(s"$direction:")
  rawLogger(":")

  var stashed = false;

  val onClose: Receive = {
    case _: ConnectionClosed =>
      logger.debug(s"${direction}X")
      rawLogger("X")
      fromConnection ! Close
      toConnection ! Close
      context.stop(self)
  }
  val waitingForData: Receive = {
    case Received(data) =>
      logger.trace(s"${direction}> ${data.length}")
      rawLogger(s"> ${data.decodeString("UTF-8")}")
      fromConnection ! SuspendReading
      toConnection ! Write(data, WriteAck(System.nanoTime(), data.length))
      context.become(writing)
  }
  val waitingForAck: Receive = {
    case Received(data) =>
      logger.trace(s"${direction}~ ${data.length}")
      stash()
      stashed = true
    case WriteAck(startTime, byteCount) =>
      logger.trace(s"${direction}+")
      rawLogger("+")
      monitorActor ! WriteDuration(System.nanoTime() - startTime, byteCount)
      stashed match {
        case true => unstashAll()
        case false => fromConnection ! ResumeReading
      }
      stashed = false
      context.become(reading)
  }
  val onUnexpected: Receive = {
    case other =>
      logger.warn(s"Unexpected $other")
      rawLogger(s"Unexpected $other")
  }
  val reading = onClose.orElse(waitingForData).orElse(onUnexpected)
  val writing = onClose.orElse(waitingForAck).orElse(onUnexpected)

  override def receive = reading
}
