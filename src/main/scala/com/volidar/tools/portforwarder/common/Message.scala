package com.volidar.tools.portforwarder.common

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, DataInputStream, DataOutputStream}

import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

object MessageTest {
  //DOIT delete
  def main(args: Array[String]) {
    val writeBuffer = new ByteArrayOutputStream()
    writeBuffer.write(Message.toBytes(Connect(1, "localhost", 3389)))
    writeBuffer.write(Message.toBytes(Noop))
    writeBuffer.write(Message.toBytes(Data(1, true, (0 to 511).map(_.toByte).toArray)))
    writeBuffer.write(Message.toBytes(Noop))
    writeBuffer.write(Message.toBytes(Disconnect(1, true)))
    val data = writeBuffer.toByteArray

    data.grouped(32).foreach(group => println(group.map(b => f"$b%02X").mkString(" ")))

    val (unparsed, commands) = Message.fromBytes(data)
    assert(unparsed.isEmpty)
    commands.foreach(println)
  }
}

sealed trait Message
case object Noop extends Message
case class Connect(connectionId: Int, host: String, port: Int) extends Message
case class Connected(connectionId: Int) extends Message
case class Disconnect(connectionId: Int, fromClient: Boolean) extends Message
case class Data(connectionId: Int, fromClient: Boolean, data: Array[Byte]) extends Message {
  override def toString = f"Data($connectionId%d, [${data.length}%s])"
//  override def toString = f"Data($connectionId%d, ${data.map(b => f"$b%02X").mkString(" ")}%s)"
}

object Message extends LazyLogging {
  private val NOOP = 0x03
  private val CONNECT = 0x00
  private val CONNECTED = 0x04
  private val DISCONNECT = 0x01
  private val DATA = 0x02

  def fromBytes(source: Array[Byte]): (Array[Byte], List[Message]) = {
    def fromBytes(source: Array[Byte], foundMessages: List[Message]): (Array[Byte], List[Message]) = {
      if(source.isEmpty) return (source, foundMessages)
      val packetSize = source(0).toInt & 0xFF
      if(source.length < packetSize + 1) return (source, foundMessages)
      try {
        source(1) match {
          case Message.NOOP => fromBytes(source.drop(packetSize + 1), Noop :: foundMessages)
          case Message.CONNECT =>
            val in = new DataInputStream(new ByteArrayInputStream(source.slice(2, packetSize + 1)))
            fromBytes(source.drop(packetSize + 1), Connect(connectionId = in.readInt(), host = in.readUTF(), port = in.readInt()) :: foundMessages)
          case Message.CONNECTED =>
            val in = new DataInputStream(new ByteArrayInputStream(source.slice(2, packetSize + 1)))
            fromBytes(source.drop(packetSize + 1), Connected(in.readInt()) :: foundMessages)
          case Message.DISCONNECT =>
            val in = new DataInputStream(new ByteArrayInputStream(source.slice(2, packetSize + 1)))
            val connectionId = in.readInt()
            val fromClient = in.readBoolean()
            fromBytes(source.drop(packetSize + 1), Disconnect(connectionId, fromClient) :: foundMessages)
          case Message.DATA =>
            val buffer = new ByteArrayInputStream(source.slice(2, packetSize + 1))
            val in = new DataInputStream(buffer)
            val connectionId = in.readInt()
            val fromClient = in.readBoolean()
            val data = source.slice(7, packetSize + 1)
            fromBytes(source.drop(packetSize + 1), Data(connectionId, fromClient, data) :: foundMessages)
        }
      }
      catch {
        case ex: Exception =>
          logger.error("Cannot parse data", ex)
          source.take(packetSize + 1).grouped(32).foreach{
            group =>
              val hex = group.map(b => f"$b%02X").mkString(" ")
              val utf = Try{new String(group, "UTF-8")}.getOrElse("<not parseable UTF-8>")
              println(s"$hex $utf")
          }
          throw ex
      }
    }
    val (notParsed, reverseMessages) = fromBytes(source, Nil)
    (notParsed, reverseMessages.reverse)
  }

  def toBytes(message: Message): Array[Byte] = {
    val buffer = new ByteArrayOutputStream(4192)
    implicit val out = new DataOutputStream(buffer)

    message match {
      case Noop => writeCommand(Message.NOOP, _ => ())
      case Connect(connectionId, host, port) => writeCommand(Message.CONNECT, o => {o.writeInt(connectionId); o.writeUTF(host); o.writeInt(port)})
      case Connected(connectionId) => writeCommand(Message.CONNECTED, _.writeInt(connectionId))
      case Disconnect(connectionId, fromClient) => writeCommand(Message.DISCONNECT, o => {o.writeInt(connectionId); o.writeBoolean(fromClient)})
      case Data(connectionId, fromClient, data) => data.grouped(248).foreach(packet => writeCommand(Message.DATA, o => {o.writeInt(connectionId); o.writeBoolean(fromClient); o.write(packet)}))
    }

    buffer.toByteArray
  }

  private def writeCommand(commandCode: Int, writer: DataOutputStream => Unit)(implicit target: DataOutputStream): Unit = {
    val commandBuffer = new ByteArrayOutputStream(255)
    val commandOut = new DataOutputStream(commandBuffer)
    commandOut.writeByte(commandCode)
    writer(commandOut)
    val commandBytes = commandBuffer.toByteArray
    target.writeByte(commandBytes.length)
    target.write(commandBytes)
  }
}
