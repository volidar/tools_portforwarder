package com.volidar.tools.portforwarder.porttopipe

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.file.{Paths, StandardOpenOption}
import java.util

import akka.actor._
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import com.volidar.tools.portforwarder.common.{Data, Disconnect, Message, Noop}
import com.volidar.tools.portforwarder.porttopipe.DispatcherActor.IncomingConnection
import com.volidar.tools.portforwarder.porttopipe.PipeReaderActor.{ReadResult, StartReading}
import com.volidar.tools.portforwarder.porttopipe.PipeWriterActor.Written

import scala.concurrent.Await
import scala.concurrent.duration._

object Main3 extends LazyLogging {
  val pipeName = """\\.\pipe\com_1"""
  val pipeSynchronizationPattern = Array[Byte](1, 3, 1, 3, 1, 3, 1, 3)

  def main(args: Array[String]) {
    val pipeChannel = AsynchronousFileChannel.open(Paths.get(pipeName), StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.SYNC);

    val system = ActorSystem("portToPipe")
    val pipeWriter = system.actorOf(PipeWriterActor.props(pipeChannel))
    val dispatcher = system.actorOf(DispatcherActor.props(pipeWriter))
    val pipeReader = system.actorOf(PipeReaderActor.props(pipeChannel, dispatcher))
//    val listener = system.actorOf(ListenerActor.props(1234, "192.168.168.1", 4321, dispatcher))
//    val listener = system.actorOf(ListenerActor.props(3391, "192.168.168.131", 3389, dispatcher))
    val listener = system.actorOf(ListenerActor.props(3391, "10.251.12.211", 3389, dispatcher))

    Await.result(system.whenTerminated, Duration.Inf)
    pipeChannel.close()
  }
}

object PipeReaderActor {
  def props(pipeChannel: AsynchronousFileChannel, dispatcher: ActorRef) = Props(classOf[PipeReaderActor], pipeChannel, dispatcher)

  case object StartReading
  case class ReadResult(data: Array[Byte])
}
class PipeReaderActor(pipeChannel: AsynchronousFileChannel, dispatcher: ActorRef) extends Actor with LazyLogging {
  val pipeSynchronizationPattern = Array[Byte](1, 3, 1, 3, 1, 3, 1, 3)
  val readBuffer = ByteBuffer.allocate(256)
  var messageBuffer = Array.empty[Byte]
  var synchronized = false

  self ! StartReading

  val readCompletionHandler = new CompletionHandler[Integer, ByteBuffer] {
    def completed(read: Integer, buffer: ByteBuffer) = {
      buffer.flip();
      val data = new Array[Byte](buffer.limit());
      buffer.get(data);
      buffer.clear();
      self ! ReadResult(data)
    }
    def failed(ex: Throwable, data: ByteBuffer) = {
      logger.error("Read failed", ex)
      context.system.terminate()
    }
  }

  override def receive = {
    case StartReading =>
//      val buffer = ByteBuffer.allocate(1024)
      pipeChannel.read(readBuffer, 0L, readBuffer, readCompletionHandler)
    case ReadResult(data) =>
      self ! StartReading
      messageBuffer ++= data
      if(!synchronized && messageBuffer.length >= pipeSynchronizationPattern.length) {
        messageBuffer.sliding(pipeSynchronizationPattern.length).zipWithIndex.collectFirst {
          case (received, index) if util.Arrays.equals(received, pipeSynchronizationPattern) => index
        } match {
          case None => messageBuffer = messageBuffer.takeRight(pipeSynchronizationPattern.length)
          case Some(synchronizationPoint) =>
            synchronized = true
            messageBuffer = messageBuffer.drop(synchronizationPoint)
        }
      }
      if(synchronized) {
        //syncronized can be changed above, so recheck it
        val (newBuffer, messages) = Message.fromBytes(messageBuffer)
        messageBuffer = newBuffer
        messages.filter(_ != Noop).foreach {
          case message =>
            logger.trace(s"<- $message")
            dispatcher ! message
        }
      }
  }
}

object PipeWriterActor {
  def props(pipeChannel: AsynchronousFileChannel) = Props(classOf[PipeWriterActor], pipeChannel)

  case object Written
}
class PipeWriterActor(pipeChannel: AsynchronousFileChannel) extends Actor with Stash with LazyLogging {
  val noopSchedule = context.system.scheduler.schedule(100.milliseconds, 100.milliseconds, self, Noop)(context.dispatcher)

  override def postStop() = noopSchedule.cancel()

  var writtingMessage = Option.empty[String]

  val waitingForData: Receive = {
    case message: Message =>
      if(message != Noop) {
        logger.trace(s"-> $message")
        writtingMessage = Some(s"$message")
      }
      val buffer = ByteBuffer.wrap(Message.toBytes(message))
      pipeChannel.write(buffer, 0L, buffer.limit(), writeCompletionHandler)
      context.become(waitingForAck)
    case Written => logger.warn("Unexpected Written")
  }
  val waitingForAck: Receive = {
    case message: Message => stash()
    case Written =>
      writtingMessage.foreach(msg => logger.trace(s"+> $msg"))
      writtingMessage = None
      unstashAll()
      context.become(waitingForData)
  }

  val writeCompletionHandler = new CompletionHandler[Integer, Int] {
    def completed(written: Integer, expected: Int) = {
      if(written == expected) {
        self ! Written
      } else {
        logger.warn(s"Writen $written != $expected")
      }
    }
    def failed(ex: Throwable, expected: Int) = {
      logger.error("Write failed", ex)
      context.system.terminate()
    }
  }

  override def receive = waitingForData
}

object ListenerActor {
  def props(sourcePort: Int, targetHost: String, targetPort: Int, dispatcher: ActorRef) = Props(classOf[ListenerActor], sourcePort, targetHost, targetPort, dispatcher)
}
class ListenerActor(sourcePort: Int, targetHost: String, targetPort: Int, dispatcher: ActorRef) extends Actor with LazyLogging {
  IO(Tcp)(context.system) ! Bind(self, new InetSocketAddress(sourcePort))
  override def receive = {
    case CommandFailed(_: Bind) =>
      logger.error(s"Cannot bind on port $sourcePort")
      context.system.terminate()
    case _: Bound => logger.debug(s"Bound on $sourcePort");
    case Connected(clientAddress, myServerAddress) =>
      logger.debug(s"Connected from $clientAddress to me at $myServerAddress")
      dispatcher ! IncomingConnection(sender(), targetHost, targetPort)
  }
}

object DispatcherActor {
  def props(pipeWriter: ActorRef) = Props(classOf[DispatcherActor], pipeWriter)
  case class IncomingConnection(socket: ActorRef, targetHost: String, targetPort: Int)
}
class DispatcherActor(pipeWriter: ActorRef) extends Actor with LazyLogging {
  var connectionIdMaster = 0
  var waitingSockets = Map.empty[Int, ActorRef]
  var connections = Map.empty[Int, ActorRef]

  override def receive = {
    case IncomingConnection(socket, targetHost, targetPort) =>
      val thisConnectionId = connectionIdMaster
      connectionIdMaster += 1
      waitingSockets += (thisConnectionId -> socket)
      pipeWriter ! com.volidar.tools.portforwarder.common.Connect(thisConnectionId, targetHost, targetPort)
    case com.volidar.tools.portforwarder.common.Connected(connectionId) =>
      waitingSockets.get(connectionId) match {
        case None =>
          connections -= connectionId
          pipeWriter ! Disconnect(connectionId, true)
        case Some(socket) =>
          waitingSockets -= connectionId
          val connection = context.actorOf(ConnectionActor.props(connectionId, socket, self))
          connections += (connectionId -> connection)
      }
    case msg@Data(_, true, _) =>
      pipeWriter ! msg
    case msg@Data(connectionId, false, _) =>
      connections.get(connectionId) match {
        case None => pipeWriter ! Disconnect(connectionId, true)
        case Some(connection) => connection ! msg
      }
    case msg@Disconnect(connectionId, true) =>
      waitingSockets -= connectionId
      connections -= connectionId
      pipeWriter ! msg
    case msg@Disconnect(connectionId, false) =>
      waitingSockets.get(connectionId).foreach {
        case socket =>
          socket ! Close
          waitingSockets -= connectionId
      }
      connections.get(connectionId).foreach {
        case connection =>
          connection ! msg
          connections -= connectionId
      }
    case msg => logger.warn(s"Unexpected message: $msg")
  }
}

object ConnectionActor {
  def props(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) = Props(classOf[ConnectionActor], connectionId, socket, dispatcher)
}
class ConnectionActor(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) extends Actor with LazyLogging {
  socket ! Register(self, false, true)
  override def receive = {
    case Data(_, false, data) => socket ! Write(ByteString(data))
    case msg@Disconnect(_, false) =>
      socket ! Close
      context.stop(self)

    case CommandFailed(_: Write) =>
      logger.error("CommandFailed(_: Write) is not expected in this implementation")
      dispatcher ! Disconnect(connectionId, fromClient = true)
      socket ! Close
      context.stop(self)
    case Received(data) =>
      dispatcher ! Data(connectionId, fromClient = true, data.toArray)
    case _: ConnectionClosed =>
      dispatcher ! Disconnect(connectionId, fromClient = true)
      context.stop(self)

    case other => logger.warn(s"Unexpected $other")
  }
}
