package com.volidar.tools.portforwarder.porttopipe

import java.io.RandomAccessFile

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object WriteToPipe extends LazyLogging{
  def main(args: Array[String]) {
    val pipeName = """\\.\pipe\com_1"""
    val pipe = new RandomAccessFile(pipeName, "rws")
    Future {
      while(true) {
        Thread.sleep(100L)
        pipe.read()
      }
    }
    Stream.continually(0 to 255).flatten.foreach{
      case i =>
        pipe.writeByte(i)
        print(f"$i%02X ")
        if(i % 32 == 31) println
    }
  }
}
