package com.volidar.tools.portforwarder.porttopipe

import java.io._
import java.net.{ServerSocket, Socket}

import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

object Main extends LazyLogging {
  val pipeName = """\\.\pipe\com_1"""

  def main(args: Array[String]) {
    val pipe = new RandomAccessFile(pipeName, "rws");
    val server = new ServerSocket(3391)
    val buffer = new Array[Byte](255)
    while(true) {
      val client = server.accept()
      logger.debug("Connection opened")
      val pipeToSocketThread = new PipeToSocket(pipe, client)
      pipeToSocketThread.start()
      try {
        val in = client.getInputStream
        while(in.read(buffer) match {
          case -1 => false
          case read =>
            logger.trace(s"-> $read")
            pipe.write(buffer, 0, read)
            true
        }) {}
      }
      catch {
        case ex: IOException => logger.debug("Exception in Socket -> Pipe", ex)
      }
      finally Try {client.close()}
      Try {
        pipeToSocketThread.interrupt()
        pipeToSocketThread.join()
      }
      logger.debug("Connection closed")
    }
  }

  class PipeToSocket(pipe: RandomAccessFile, socket: Socket) extends Thread {
    val out = socket.getOutputStream
    override def run() = {
      val buffer = new Array[Byte](256)
      var rxPos = 0
      try {
        while(!isInterrupted) {
          pipe.read(buffer, rxPos, buffer.length - rxPos) match {
            case -1 => interrupt()
            case 0 => ()
            case read =>
              Main.logger.trace(s"<- $read")
              rxPos += read
              while (rxPos >= 1 + (buffer(0).toInt & 0xFF)) {
                val packetSize = buffer(0).toInt & 0xFF
                out.write(buffer, 1, packetSize)
                System.arraycopy(buffer, 1 + packetSize, buffer, 0, buffer.length - 1 - packetSize)
                rxPos -= 1 + packetSize
              }
          }
        }
      }
      catch {
        case ex: IOException => logger.debug("Exception in Pipe -> Socket", ex)
      }
      finally {
        out.close()
      }
    }
  }
}


