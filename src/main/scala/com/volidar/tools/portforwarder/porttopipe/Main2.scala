//package com.volidar.tools.portforwarder.porttopipe
//
//import java.io.RandomAccessFile
//import java.net.InetSocketAddress
//import java.util
//
//import akka.actor.{Actor, ActorRef, ActorSystem, Props}
//import akka.io.Tcp._
//import akka.io.{IO, Tcp}
//import akka.util.ByteString
//import com.typesafe.scalalogging.LazyLogging
//import com.volidar.tools.portforwarder.common.{Data, Disconnect, Message, Noop}
//import com.volidar.tools.portforwarder.porttopipe.DispatcherActor.IncomingConnection
//
//class NamedPipe(name: String) {
//  private val pipe = new RandomAccessFile(name, "rws")
//
//  def read(buffer: Array[Byte], offset: Int, length: Int) = {
//    synchronized{
//      pipe.read(buffer, offset, length)
//    }
//  }
//  def writeByte(byte: Int) = {
//    synchronized{
//      pipe.writeByte(byte)
//    }
//  }
//  def write(bytes: Array[Byte]) = {
//    synchronized{
//      pipe.write(bytes)
//    }
//  }
//}
//
//object Main2 extends LazyLogging {
//  val pipeName = """\\.\pipe\com_1"""
//  val pipeSynchronizationPattern = Array[Byte](1, 3, 1, 3, 1, 3, 1, 3)
//
//  def main(args: Array[String]) {
//    val pipe = new NamedPipe(pipeName)
//    val system = ActorSystem("portToPipe")
//    val pipeWriter = system.actorOf(PipeWriterActor.props(pipe))
//    val dispatcher = system.actorOf(DispatcherActor.props(pipeWriter))
//    val listener = system.actorOf(ListenerActor.props(1234, "192.168.168.1", 4321, dispatcher))
////    val listener = system.actorOf(ListenerActor.props(3391, "192.168.168.131", 3389, dispatcher))
//
//    try {
//      var rxPos = 0
//      val buffer = new Array[Byte](512)
//      var synchronized = false
//      while(true) {
////        logger.trace("?")
//        pipe.read(buffer, rxPos, buffer.length - rxPos) match {
//          case -1 => throw new Exception("Pipe closed")
//          case 0 => ()
//          case read =>
//            logger.trace(s"! $read")
//            rxPos += read
//            if(!synchronized && rxPos >= pipeSynchronizationPattern.length) {
//              buffer.take(rxPos).sliding(pipeSynchronizationPattern.length).zipWithIndex.collectFirst {
//                case (received, index) if util.Arrays.equals(received, pipeSynchronizationPattern) => index
//              } match {
//                case None =>
//                  System.arraycopy(buffer, rxPos - pipeSynchronizationPattern.length, buffer, 0, pipeSynchronizationPattern.length)
//                  rxPos = pipeSynchronizationPattern.length
//                case Some(synchronizationPoint) =>
//                  synchronized = true
//                  System.arraycopy(buffer, synchronizationPoint, buffer, 0, rxPos - synchronizationPoint)
//                  rxPos -= synchronizationPoint
//              }
//            }
//            if(synchronized) {
//              //syncronized can be changed above, so recheck it
//              val (newBuffer, messages) = Message.fromBytes(buffer.take(rxPos))
//              rxPos = newBuffer.length
//              System.arraycopy(newBuffer, 0, buffer, 0, rxPos)
//              messages.filter(_ != Noop).foreach {
//                case message =>
//                  logger.trace(s"<- $message")
//                  dispatcher ! message
//              }
//            }
//        }
//      }
//    }
//    catch {
//      case ex: Exception => logger.error("Error reading pipe", ex)
//    }
//    system.terminate()
//  }
//}
//
//object PipeWriterActor {
//  def props(pipe: NamedPipe) = Props(classOf[PipeWriterActor], pipe)
//}
//class PipeWriterActor(pipe: NamedPipe) extends Actor with LazyLogging {
//
//  import scala.concurrent.duration._
//
//  val noopSchedule = context.system.scheduler.schedule(1000.milliseconds, 1000.milliseconds, self, Noop)(context.dispatcher)
//
//  override def receive = {
//    case message: Message =>
//      /*if(message != Noop)*/ logger.trace(s"-> $message")
//      val start = System.currentTimeMillis()
////      pipe.write(Message.toBytes(message))
//      pipe.writeByte(1)
//      pipe.writeByte(3)
//      /*if(message != Noop)*/ logger.trace(s"+> (${System.currentTimeMillis() - start})")
//  }
//}
//
//object ListenerActor {
//  def props(sourcePort: Int, targetHost: String, targetPort: Int, dispatcher: ActorRef) = Props(classOf[ListenerActor], sourcePort, targetHost, targetPort, dispatcher)
//}
//class ListenerActor(sourcePort: Int, targetHost: String, targetPort: Int, dispatcher: ActorRef) extends Actor with LazyLogging {
//  IO(Tcp)(context.system) ! Bind(self, new InetSocketAddress(sourcePort))
//  override def receive = {
//    case CommandFailed(_: Bind) =>
//      logger.error(s"Cannot bind on port $sourcePort")
//      context.system.terminate()
//    case _: Bound => logger.debug(s"Bound on $sourcePort");
//    case Connected(clientAddress, myServerAddress) =>
//      logger.debug(s"Connected from $clientAddress to me at $myServerAddress")
//      dispatcher ! IncomingConnection(sender(), targetHost, targetPort)
//  }
//}
//
//object DispatcherActor {
//  def props(pipeWriter: ActorRef) = Props(classOf[DispatcherActor], pipeWriter)
//  case class IncomingConnection(socket: ActorRef, targetHost: String, targetPort: Int)
//}
//class DispatcherActor(pipeWriter: ActorRef) extends Actor with LazyLogging {
//  var connectionIdMaster = 0
//  var waitingSockets = Map.empty[Int, ActorRef]
//  var connections = Map.empty[Int, ActorRef]
//
//  override def receive = {
//    case IncomingConnection(socket, targetHost, targetPort) =>
//      val thisConnectionId = connectionIdMaster
//      connectionIdMaster += 1
//      waitingSockets += (thisConnectionId -> socket)
//      pipeWriter ! com.volidar.tools.portforwarder.common.Connect(thisConnectionId, targetHost, targetPort)
//    case com.volidar.tools.portforwarder.common.Connected(connectionId) =>
//      waitingSockets.get(connectionId) match {
//        case None =>
//          connections -= connectionId
//          pipeWriter ! Disconnect(connectionId, true)
//        case Some(socket) =>
//          waitingSockets -= connectionId
//          val connection = context.actorOf(ConnectionActor.props(connectionId, socket, self))
//          connections += (connectionId -> connection)
//      }
//    case msg@Data(_, true, _) =>
//      pipeWriter ! msg
//    case msg@Data(connectionId, false, _) =>
//      connections.get(connectionId) match {
//        case None => pipeWriter ! Disconnect(connectionId, true)
//        case Some(connection) => connection ! msg
//      }
//    case msg@Disconnect(connectionId, true) =>
//      waitingSockets -= connectionId
//      connections -= connectionId
//      pipeWriter ! msg
//    case msg@Disconnect(connectionId, false) =>
//      waitingSockets.get(connectionId).foreach {
//        case socket =>
//          socket ! Close
//          waitingSockets -= connectionId
//      }
//      connections.get(connectionId).foreach {
//        case connection =>
//          connection ! msg
//          connections -= connectionId
//      }
//    case msg => logger.warn(s"Unexpected message: $msg")
//  }
//}
//
//object ConnectionActor {
//  def props(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) = Props(classOf[ConnectionActor], connectionId, socket, dispatcher)
//}
//class ConnectionActor(connectionId: Int, socket: ActorRef, dispatcher: ActorRef) extends Actor with LazyLogging {
//  socket ! Register(self, false, true)
//  override def receive = {
//    case Data(_, false, data) => socket ! Write(ByteString(data))
//    case msg@Disconnect(_, false) =>
//      socket ! Close
//      context.stop(self)
//
//    case CommandFailed(_: Write) =>
//      logger.error("CommandFailed(_: Write) is not expected in this implementation")
//      dispatcher ! Disconnect(connectionId, fromClient = true)
//      socket ! Close
//      context.stop(self)
//    case Received(data) =>
//      dispatcher ! Data(connectionId, fromClient = true, data.toArray)
//    case _: ConnectionClosed =>
//      dispatcher ! Disconnect(connectionId, fromClient = true)
//      context.stop(self)
//
//    case other => logger.warn(s"Unexpected $other")
//  }
//}
