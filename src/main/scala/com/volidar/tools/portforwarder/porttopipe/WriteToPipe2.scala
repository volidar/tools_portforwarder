package com.volidar.tools.portforwarder.porttopipe

import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.file.{Paths, StandardOpenOption}

import akka.actor.{Actor, ActorSystem, Props, Stash}
import com.typesafe.scalalogging.LazyLogging
import com.volidar.tools.portforwarder.porttopipe.PipeReader.{ReadResult, StartReading}
import com.volidar.tools.portforwarder.porttopipe.PipeWriter.{Data, Written}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}

object WriteToPipe2 extends LazyLogging {
  def main(args: Array[String]) {
    val pipeName = """\\.\pipe\com_1"""
    val path = Paths.get(pipeName);

    val pipeChannel = AsynchronousFileChannel.open(path, StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.SYNC);
    val system = ActorSystem("WriteToPipe2")
    val pipeWriter = system.actorOf(PipeWriter.props(pipeChannel))
    val pipeReader = system.actorOf(PipeReader.props(pipeChannel))
    system.scheduler.schedule(1.second, 1.second, pipeWriter, Data((0 to 255).map(_.toByte).toArray))(system.dispatcher)
    Await.result(system.whenTerminated, Duration.Inf)
    pipeChannel.close()
  }
}

object PipeReader {
  def props(pipeChannel: AsynchronousFileChannel) = Props(classOf[PipeReader], pipeChannel)

  case object StartReading
  case class ReadResult(data: Array[Byte])
}
class PipeReader(pipeChannel: AsynchronousFileChannel) extends Actor with LazyLogging {
  self ! StartReading

  override def receive = {
    case StartReading =>
      val buffer = ByteBuffer.allocate(1024)
      pipeChannel.read(buffer, 0L, buffer, readCompletionHandler)
    case ReadResult(data) =>
      logger.debug(new String(data, "UTF-8"))
      self ! StartReading
  }

  val readCompletionHandler = new CompletionHandler[Integer, ByteBuffer] {
    def completed(read: Integer, buffer: ByteBuffer) = {
      buffer.flip();
      val data = new Array[Byte](buffer.limit());
      buffer.get(data);
      buffer.clear();
      self ! ReadResult(data)
    }
    def failed(ex: Throwable, data: ByteBuffer) = {
      logger.error("Read failed", ex)
      context.system.terminate()
    }
  }
}

object PipeWriter {
  def props(pipeChannel: AsynchronousFileChannel) = Props(classOf[PipeWriter], pipeChannel)

  case class Data(data: Array[Byte])
  case object Written
}
class PipeWriter(pipeChannel: AsynchronousFileChannel) extends Actor with Stash with LazyLogging {
  val waitingForData: Receive = {
    case Data(data) =>
      val buffer = ByteBuffer.wrap(data)
      pipeChannel.write(buffer, 0L, data.length, writeCompletionHandler)
      context.become(waitingForAck)
    case Written => logger.warn("Unexpected Written")
  }
  val waitingForAck: Receive = {
    case msg: Data =>
      stash()
    case Written =>
      unstashAll()
      context.become(waitingForData)
  }

  val writeCompletionHandler = new CompletionHandler[Integer, Int] {
    def completed(written: Integer, expected: Int) = {
      if(written == expected) {
        self ! Written
      } else {
        logger.warn(s"Writen $written != $expected")
      }
    }
    def failed(ex: Throwable, expected: Int) = {
      logger.error("Write failed", ex)
      context.system.terminate()
    }
  }

  override def receive = waitingForData
}
