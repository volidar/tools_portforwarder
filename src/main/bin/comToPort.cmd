@ECHO OFF
:: $Id: $

SETLOCAL EnableDelayedExpansion
CLS

IF "%JAVA_HOME%" == "" SET JAVA_HOME=jdk

SET CLASSPATH=lib\*
SET JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=UTF-8 -server -classpath "%CLASSPATH%"
PUSHD ..
"%JAVA_HOME%/bin/java" %JAVA_OPTS% com.volidar.tools.portforwarder.comtoport.Main2 %*
POPD

ENDLOCAL
@ECHO ON
